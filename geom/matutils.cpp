#include "matutils.h"

static inline int max(int x, int y) {
    return x > y? x : y;
}

static inline int min(int x, int y) {
    return x < y? x : y;
}

void zero(mat3 &M) {
    for (int i=0; i < 3; i++) {
        for (int j=0; j < 3; j++) {
            M[i][j] = 0;
        }
    }
}

double dotProd(point &X, point &Y) {
  return (X.x * Y.x) + (X.y * Y.y) + (X.z * Y.z);
}

void subtract(point &X, point &Y) {
  X.x -= Y.x;
  X.y -= Y.y;
  X.z -= Y.z;
}

void scale(point &X, double s) {
  X.x *= s;
  X.y *= s;
  X.z *= s;
}

void transform(mat3 &T, point &X, point *Y) {
  Y->x = T[0][0]*X.x + T[0][1]*X.y + T[0][2]*X.z;
  Y->y = T[1][0]*X.x + T[1][1]*X.y + T[1][2]*X.z;
  Y->z = T[2][0]*X.x + T[2][1]*X.y + T[2][2]*X.z;
}

void transform(mat3 &T, point &X) {
  double x = X.x, y = X.y, z = X.z;
  X.x = T[0][0]*x + T[0][1]*y + T[0][2]*z;
  X.y = T[1][0]*x + T[1][1]*y + T[1][2]*z;
  X.z = T[2][0]*x + T[2][1]*y + T[2][2]*z;
}

point* projectPoint(point &c, point &p, mat3 basis) {
  return 0;
}

void inverse(mat3 &M, mat3 &I) {
  double det = 0, sign;
  int i,j,w,x,y,z;
  for (i=0; i<3; i++) {
      det += (M[0][i]*(M[1][(i+1)%3]*M[2][(i+2)%3] -
              M[1][(i+2)%3]*M[2][(i+1)%3]));
  }
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
        w = min((i+1)%3, (i+2)%3);
        x = max((i+1)%3, (i+2)%3);
        y = min((j+1)%3, (j+2)%3);
        z = max((j+1)%3, (j+2)%3);
        sign = (i+j) % 2? -1.0 : 1.0; 
        I[j][i] = sign * (1.0/det) * (M[w][y]*M[x][z] - M[w][z]*M[x][y]);
    }
  }
}
