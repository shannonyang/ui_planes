#include "convexhull.h"

/*
 * Implementation of monotone chain for a vector of floats
 */

// Computes direction of the turn from last2 -> last1 -> p
// -1 is cc, 1 is clockwise, 0 is straight (hard to handle
// with floats, but settling for now).

int direction (point* last2, point* last1, point* p) {
    float val = (last1->x - last2->x)*(p->y - last2->y) -
                (last1->y - last2->y)*(p->x - last2->x);
    if (val == 0) {
        return 0;
    }
    return (val < 0)? -1 : 1;
}

bool compare(point* p1, point* p2) {
    return (p1->x < p2->x) || (p1->x == p2->x && p1->y < p2->y);
}

XYZCloud convex_hull(XYZCloud &points, XYZCloud &hull) {
    int n = points.size(), k = 0;
    hull.resize(2*n);
    std::sort(points.begin(), points.end(), compare);
    for (int i = 0; i < n; ++i) {
        while (k >= 2 && direction(hull[k-2], hull[k-1], points[i]) >= 0) {
            k--;
        }
        hull[k++] = points[i];
    }
    for (int i = n-2, t = k+1; i >= 0; i--) {
        while (k >= t && direction(hull[k-2], hull[k-1], points[i]) >= 0) {
            k--;
        }
        hull[k++] = points[i];
    }

    hull.resize(k-1);
    return hull;
}